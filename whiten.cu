#include<iostream>
#include<stdio.h>
#include<complex>

void waveform_normalize( double deltaF, int N, std::complex<double> *wf, int N_psd, double *PSD){

    double Sum = 0;
    for(int j=0; j<N; j++) Sum += norm(wf[j])/PSD[j] ;
    Sum = 2.0*sqrt( Sum*deltaF);
    if( Sum==0.0)printf("waveform generate ERROR\n");
    for(int j=0; j<N; j++) wf[j] /= Sum ;
}
