
#include<iostream>
#include<complex>
#include<cuComplex.h>
#include<cufft.h>
#include<cublas_v2.h>
#include<cuda_runtime.h>
#include"helper_cuda.h"

#include"calloc.hu"

#define NTH 256
#define Ctype cuDoubleComplex
typedef std::complex<double> Complex;

#define CHECKBLAS(call) \
{ \
    const cublasStatus_t error = call; \
    if (error != CUBLAS_STATUS_SUCCESS) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%s\n", _cudaGetErrorEnum(error)); \
        exit(1); \
    } \
}
#define CHECKFFT(call) \
{ \
    const cufftResult error = call; \
    if (error != CUFFT_SUCCESS) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%s\n", _cudaGetErrorEnum(error)); \
        exit(1); \
    } \
}
#define CHECK(call) \
{ \
    const cudaError_t error = call; \
    if (error != cudaSuccess) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%d, reason: %s\n", error, cudaGetErrorString(error)); \
        exit(1); \
    } \
}
static __global__ void ComplexMulti( int n_chunk, int npoint, Ctype *a, Ctype *b, Ctype *out){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint) return;
    Ctype *o_Ptr = out;
    Ctype *b_Ptr = b;
    for(int i=0; i<n_chunk; i++){
        o_Ptr[id] = cuCmul( cuConj(a[id]), b_Ptr[id]); 
        o_Ptr += npoint;
        b_Ptr += npoint;
    }
}
static __global__ void SNR_calculate( int n_chunk, int npoint, int overlap,
        double *a, double *b, double *o){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint-overlap) return;
    int shift = overlap/2;
    double *a_Ptr = a + shift;
    double *b_Ptr = b + shift;
    double *o_Ptr = o + shift;
    for(int i=0; i<n_chunk; i++){
        double tmp = sqrt( a_Ptr[id]*a_Ptr[id] + b_Ptr[id]*b_Ptr[id]);
        if( o_Ptr[id] < tmp) o_Ptr[id] = tmp;
        a_Ptr += npoint;
        b_Ptr += npoint;
        o_Ptr += npoint-overlap;
    }
}

void matchedfilter( int size_f,
        int n_signal, Complex *signal,
        int n_data, Complex *data,
        int overlap, int npoint, double *snr_out){

    int n_chunk = n_data/size_f;
    Complex *data_d = (Complex*)malloc_d(sizeof(Complex)*n_chunk*size_f);
  //CHECK( cudaMemcpy( data_d, data, sizeof(Complex)*n_chunk*size_f, cudaMemcpyHostToDevice));
    CHECKBLAS( cublasSetVector( size_f*n_chunk, sizeof(Complex), data, 1, data_d, 1));

    int size = size_f*2-2;
  //printf("size = %d size_f = %d overlap = %d\n", size, size_f,overlap);
    double *fft_out_r = (double*)malloc_d(sizeof(double)*n_chunk*size);
    double *fft_out_i = (double*)malloc_d(sizeof(double)*n_chunk*size);
    Ctype *fft_in = (Ctype*)malloc_d(sizeof(Ctype)*n_chunk*size_f); 

    cufftHandle plan;
    CHECKFFT( cufftPlan1d( &plan, size, CUFFT_Z2D, n_chunk));

    cublasHandle_t handle;
    CHECKBLAS( cublasCreate( &handle));

    double *snr_max = (double*)malloc_d(sizeof(double)*npoint);
    CHECK( cudaMemcpy( snr_max, snr_out, sizeof(double)*npoint, cudaMemcpyHostToDevice)); 

    Complex *signal_d = (Complex*)malloc_d(sizeof(Complex)*size_f);
    Complex *signal_Ptr = signal;
    for(int i=0; i<n_signal/size_f; i++){

      //CHECKBLAS( cublasSetVector( size_f, sizeof(Complex), signal_Ptr, 1, signal_d, 1));
        CHECK( cudaMemcpy( signal_d, signal_Ptr, sizeof(Ctype)*size_f, cudaMemcpyHostToDevice));

        int block = (size_f+NTH-1)/NTH;
        ComplexMulti <<< block, NTH>>>( n_chunk, size_f, (Ctype*)signal_d, (Ctype*)data_d, fft_in);
        CHECK( cudaDeviceSynchronize());

        CHECKFFT( cufftExecZ2D( plan, fft_in, fft_out_r));

        CHECK( cudaDeviceSynchronize());
        Ctype I = make_cuDoubleComplex( 0.0, -1.0);
        CHECKBLAS( cublasZscal( handle, n_chunk*size_f, &I, (Ctype*)fft_in, 1));
        CHECK( cudaDeviceSynchronize());

        CHECKFFT( cufftExecZ2D( plan, fft_in, fft_out_i));
        CHECK( cudaDeviceSynchronize());

        int block_c = (size-overlap+NTH-1)/NTH;
        SNR_calculate <<< block_c, NTH>>>
            ( n_chunk, size, overlap, fft_out_r, fft_out_i, snr_max);
        CHECK( cudaDeviceSynchronize());


        signal_Ptr += size_f;
    }


    CHECK( cudaMemcpy( snr_out, snr_max, sizeof(double)*npoint, cudaMemcpyDeviceToHost));


  //double *a = (double*)malloc_u(sizeof(double)*size);
  //CHECK( cudaMemset( a, 0, sizeof(double)*size)); 
  //cuprint <<< 1, NTH>>>( size, a);
  //cudaDeviceSynchronize();
  //for(int i=0; i<NTH; i++) printf("cpu a[%d] = %g\n", i, a[i]);
  //CHECK( cudaFree( a));



    CHECK( cudaFree( data_d));
    CHECK( cudaFree( signal_d));

    CHECK( cudaFree( fft_out_r));
    CHECK( cudaFree( fft_out_i));
    CHECK( cudaFree( fft_in));

    CHECKFFT( cufftDestroy( plan));
    CHECKBLAS( cublasDestroy( handle));
    CHECK( cudaFree( snr_max));
}
