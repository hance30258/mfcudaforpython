CPP = g++ -fPIC -O2
NVCC = nvcc -Xcompiler -fopenmp -Xcompiler -fPIC -O3
SWIG = swig -c++ -python

# change the direction if needed
CUDA_PATH = /usr/local/cuda
NUMPY_INCLUDE_PATH = /usr/lib64/python2.7/site-packages/numpy/core/include

CUDA_FLAGS = -lcudart -lcuda -lcufft -lcublas -L$(CUDA_PATH)/lib64 -I$(CUDA_PATH)/include
PYTHON_FLAGS = -I/usr/include/python2.7 -I$(NUMPY_INCLUDE_PATH)
CFLAGS = $(CUDA_FLAGS)

pymodule_name = cuda_mf
object = calloc.o cprint.o whiten.o mf.o $(pymodule_name)_wrap.o
shared_object = _$(pymodule_name).so

$(shared_object): $(object)
	g++ -shared $^ -o $@ $(CFLAGS)

%.o: %.cpp
	$(CPP) -c -o $@ $^
%.o: %.cu
	$(NVCC) -c -o $@ $^ $(CUDA_FLAGS)
%.o: %.cxx
	$(CPP) -c -o $@ $^ $(PYTHON_FLAGS)
%_wrap.cxx: %.i
	$(SWIG) $^

all: $(shared_object)

clean:
	rm *.o *.so $(pymodule_name).py $(pymodule_name).pyc
