
%module cuda_mf
%include <std_complex.i>

%{
#define SWIG_FILE_WITH_INIT
#include "cprint.hu"
#include "whiten.hpp"
#include "mf.hu"
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply(int DIM1, double* IN_ARRAY1) {(int N, double *data)}
%apply(int DIM1, std::complex<double>* IN_ARRAY1) {(int N, std::complex<double> *data)}

%apply(int DIM1, std::complex<double>* INPLACE_ARRAY1) {(int n_signal, std::complex<double> *signal)}
%apply(int DIM1, std::complex<double>* INPLACE_ARRAY1) {(int n_data, std::complex<double> *dataf)}
%apply(int DIM1, double* INPLACE_ARRAY1) {(int npoint, double *snr_out)}
%include "cprint.hu"
%apply(int DIM1, double* INPLACE_ARRAY1) {(int N_psd, double *PSD)}
%apply(int DIM1, std::complex<double>* INPLACE_ARRAY1) {(int N, std::complex<double> *wf)}
%include "whiten.hpp"
#include "mf.hu"
